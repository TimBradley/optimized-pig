//This class calculates the optimal decision based on the current state for each state possible in the game pig. 
public class PigSolver {
    int goal;
    double epsilon;
    double[][][] p;
    boolean[][][] roll;

    
    PigSolver(int goal, double epsilon) {
        this.goal = goal;
        this.epsilon = epsilon;
        p = new double[goal][goal][goal];
        roll = new boolean[goal][goal][goal];
        valueIterate();
    }

    //This function decides to roll or hold based on stored values calculated previously.
    void valueIterate() {
      double maxChange;
        do {
            maxChange = 0.0;
            for (int i = 0; i < goal; i++) // for all i
                for (int j = 0; j < goal; j++) // for all j
                    for (int k = 0; k < goal - i; k++) { // for all k
                        double oldProb = p[i][j][k];
                        double pRoll = (1.0 - pWin(j, i, 0) + pWin(i, j, k + 2) + pWin(i, j, k + 3)+ pWin(i, j, k + 4)+ pWin(i, j, k + 5)+ pWin(i, j, k + 6)) / 6;
                        double pHold = 1.0 - pWin(j, i + k, 0);
                        p[i][j][k] = Math.max(pRoll, pHold);
                        roll[i][j][k] = pRoll > pHold;
                        double change = Math.abs(p[i][j][k] - oldProb);
                        maxChange = Math.max(maxChange, change);
                    }
        } while (maxChange >= epsilon);
    }

    //Recognizes when a win occurs, if the win condition is not met return the probability for the given state
    public double pWin(int i, int j, int k) { 
        if (i + k >= goal)
            return 1.0;
        else if (j >= goal)
            return 0.0;
        else return p[i][j][k];
    }

    //Function that outputs the optimal hold values for each state
    public void outputHoldValues() {
        for (int i = 0; i < goal; i++) {
            for (int j = 0; j < goal; j++) {
                int k = 0;
                while (k < goal - i && roll[i][j][k])
                    k++;
                if (j != 99){    
                System.out.print(k + ",");
                }
                else {
                System.out.print(k);
                }
            }
            System.out.println();
        }
        }
    
    //Runs the PigSolver function with the score of 100 as an input and an epsilon value of 1e-9 which limits the amount of iterations that occur
    public static void main(String[] args){
        new PigSolver(100, 1e-9).outputHoldValues();
    }
}

import numpy as np
import pandas as pd
import matplotlib
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import os

#needs an environment with pandas and matplotlib installed 

def main():
    #gets the current working directory and looks for a file called pighold.csv
    cwd = os.getcwd()
    df = pd.read_csv(cwd + '\\pighold.csv')
    #initialized arrays used for the 3d plot
    X = np.array([])
    Y = np.array([])
    Z = np.array([])
    #give the arrays our input data with x representing the i values, y representing the j values, and z representing our hold values
    for i in range(df.index.size):
        X = np.concatenate([X, np.full(df.columns.size, df.index[i])], 0)
    for i in range(df.index.size):
        Y = np.concatenate([Y, np.array(df.columns)], 0)
    for i in range(df.index.size):
        Z = np.concatenate([Z, np.array(df[i:i+1])[0]], 0)
    #generates a scatter plot with the data generated from the pig program
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(X, Y, Z)
    ax.set_xlabel("i")
    ax.set_ylabel('j')
    ax.set_zlabel('turn holding value')
    plt.yticks([19,39,59,79,99])
    plt.show()

main()

